# Codebase release 1.3 for SymTensor

by Yang Gao, Phillip Helms, Garnet Kin-Lic Chan, Edgar Solomonik

SciPost Phys. Codebases 10-r1.0 (2023) - published 2023-02-24

[DOI:10.21468/SciPostPhysCodeb.10-r1.3](https://doi.org/10.21468/SciPostPhysCodeb.10-r1.3)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.10-r1.3) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Yang Gao, Phillip Helms, Garnet Kin-Lic Chan, Edgar Solomonik

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.10-r1.3](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.10-r1.3)
* Live (external) repository at [https://github.com/yangcal/symtensor/tree/cb14fe567a629d5c992f31c499489b3b39bc6a1f](https://github.com/yangcal/symtensor/tree/cb14fe567a629d5c992f31c499489b3b39bc6a1f)
